﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class League
    {
        [Key]
        public int LeagueID { get; set; }

        [Required(ErrorMessage = "The field {0} is requeried")]
        [MaxLength(50, ErrorMessage = "The maximun length for field {0} is {1} characteres")]
        [Index("League_Name_Index", IsUnique = true)]
        [Display(Name = "League")]
        public string Name { get; set; }
        [DataType(DataType.ImageUrl)]
        public string logo { get; set; }

        public virtual ICollection<Team> Teams { get; set; }
    }
}
