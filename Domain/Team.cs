﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Team
    {
        [Key]
        public int TeamID { get; set; }

        [Required(ErrorMessage = "The field {0} is requeried")]
        [MaxLength(50, ErrorMessage = "The maximun length for field {0} is {1} characteres")]
        [Index("Team_Name_LeagueId_Index", IsUnique = true)]
        [Display(Name = "Team")]
        public string Name { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Logo { get; set; }

        [Required(ErrorMessage = "The field {0} is requeried")]
        [StringLength(3, ErrorMessage = "The maximun length for field {0} is {1} characteres", MinimumLength = 3)]
        [Index("Team_Initials_LeagueId_Index", IsUnique = true, Order = 1)]
        public string Initials { get; set; }

        [Index("Team_Name_LeagueId_Index", IsUnique = true, Order = 2)]
        [Index("Team_Initials_LeagueId_Index", IsUnique = true, Order = 2)]
        [Display(Name = "League")]
        public int LeagueId { get; set; }

        public virtual League League { get; set; }

        public virtual ICollection<TournamentTeam> TournamentTeams { get; set; }
    }
}
