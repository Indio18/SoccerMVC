﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SoccerBack.Startup))]
namespace SoccerBack
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
